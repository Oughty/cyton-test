#clone the project
git clone https://Oughty@bitbucket.org/Oughty/cyton-test.git

#install depencies
composer install

#edit the environment file

#run migrations to create the tables
php artisan migrate

#seed the Database
php artisan db:seed

three users will be created, the login for the admin user
email: eric@oti.com
password: otieno

Once the admin has logged in he/she can create other users, in the user management section

#run the Worker
php artisan queue:work

This will be responsible for sending emails to assignees of tasks once a task has been created.

Uses the details set in the environment file, ie the mail driver, username etc.

<?php

namespace App\Service;

use App\Department;
use App\Task;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class DepartmentService {

    const ITEM_PER_PAGE = 15;

    public function show($priority, $department) {
        $userID = Auth::id();
        $tasks = Department::all()
                ->find($department)
                ->tasks();
        /*$tasks =
        DB::table('tasks')->where('department_id', '=', $department)
        ->join('users', 'tasks.assignee_id', '=', 'users.id')
        ->get(); */

        if ($priority != 'any') {
            $tasks = $tasks->where('priority', '=', $priority);
        }
        //dd($tasks);
        $tasks = $tasks->paginate(static::ITEM_PER_PAGE);
        return $tasks;
    }

    public function fetchAll() {

        $departments = Department::all();
        return $departments;
    }

    public function getTaskCount($department) {
        $totalTasks = $tasks = Department::find($department)->tasks()->count();
        return $totalTasks;
    }

    public function create(array $departmentData) {
        $department = new Department();
        $department->title = $departmentData['title'];
        $department->description = $departmentData['description'];
        $department->created_at = \Carbon\Carbon::now();
        $department->created_at = \Carbon\Carbon::now();
        $department->user_id = Auth::id();

        return $department->save();
    }

}

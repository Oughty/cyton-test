<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;
use App\Service\DepartmentService;
use App\Service\TaskService;

class DepartmentController extends Controller {

    private $departmentService;
    private $taskService;

    public function __construct(DepartmentService $departmentService, TaskService $taskService) {
        $this->departmentService = $departmentService;
        $this->taskService = $taskService;
        //All Task controler action should be taken from authenticated user
        $this->middleware('auth');
    }

    public function index($department, $priority) {

    }

    public function create() {
        $departments = $this->departmentService->fetchAll();
        return view('departments.create', ['departments' => $departments]);
    }

    public function store(Request $request) {
        $this->validate($request, $this->validationRules());
        $newdepartment = $this->departmentService->create($request->all());

        return redirect()->route('dashboard')
        ->with('flash_message', 'Task created');
    }

    public function show(Department $department, $priority) {
        $tasks = $this->departmentService->show($priority, $department->id);
        $totalCountByPriority = $this->taskService->getPriorityCount($department->id);
        return view('tasks.index', ['tasks' => $tasks
            , 'totalByPriority' => $totalCountByPriority
            , 'priority' => $priority
            , 'department' => $department->id
            , 'title' => $department->title
          ]);
    }

    public function edit(Sheet $sheet) {
        //
    }
    public function update(Request $request, Sheet $sheet) {
        //
    }
    public function destroy(Sheet $sheet) {
        //
    }
    private function validationRules()
    {
        return [

            'title'=>'required',
            'description' => 'required'
        ];
    }

}

<?php

namespace App\Http\Controllers;

use App\User;
use App\Department;
use App\Task;
use Illuminate\Http\Request;
use App\Service\TaskService;
use App\Service\DepartmentService;
use App\Jobs\SendNotificationEmailsJob;

class TaskController extends Controller {

    private $taskService;
    private $departmentService;

    /**
     * Authenticate each action
     * @param TaskService $taskService
     */
    public function __construct(TaskService $taskService, DepartmentService $departmentService) {
        $this->taskService = $taskService;
        $this->departmentService = $departmentService;
        //All Task controler action should be taken from authenticated user
        $this->middleware('auth');
    }

    public function index($department, $priority = 'any') {

    }

    public function create($department) {

      $departments = $this->departmentService->fetchAll();
      $title = Department::where('id',$department)->pluck('title')[0];
      $assignees = User::all();
      $priorityOptions = ['urgent', 'high', 'medium', 'low'];
      return view('tasks.create',
      ['departments' => $departments,
      'priorityOptions' => $priorityOptions,
      'department'=>$department,
      'title'=>$title,
      'assignees' =>$assignees]);
    }

    public function store(Request $request, $department) {
        //dd($request->all());
        $this->validate($request, $this->validationRules());
        $departments = $this->departmentService->fetchAll();
        $priorityOptions = ['urgent', 'high', 'medium', 'low'];
        $newtask=$this->taskService->create($department, $request->all());

        SendNotificationEmailsJob::dispatch($newtask);
        return redirect()->route('dashboard')
        ->with('flash_message', 'Task created');
        }

    public function show(Task $task) {
      return view('tasks.show', ['tasks'=>$task]);
    }

    public function edit(Task $task) {
        //
    }

    public function update(Request $request, Task $task) {
        //
    }

    public function destroy(Task $task) {
        //
    }
    private function validationRules()
    {
        return [

            'title' => 'required',
            'content' => 'required',
            'dueDate' => 'required',
            'assignee' => 'required'
        ];
    }

}

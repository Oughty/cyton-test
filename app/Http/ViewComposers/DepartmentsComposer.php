<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Department;
use App\Service\DepartmentService;

class DepartmentsComposer
{
    public $departments;
    private $departmentService;
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct(DepartmentService $departmentService)
    {
        $this->departmentService = $departmentService;
        $departments = $this->departmentService->fetchAll();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $departments = $this->departmentService->fetchAll();
        $view->with(['departments' => $departments]);
    }
}

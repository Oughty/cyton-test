<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
  public static function defaultPermissions()
  {
      return
          [
              'Administer roles & permissions',
              'Add Department',
              'Manage Users',
              'View Tasks',
          ];
  }
}

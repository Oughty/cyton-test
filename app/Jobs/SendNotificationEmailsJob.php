<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use App\User;
use App\Task;
use App\Mail\AssignmentMail;

class SendNotificationEmailsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $newtask;

    public function __construct(Task $newtask)
    {
        $this->newtask=$newtask;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->NotifyAssignees();
    }

    private function NotifyAssignees () {
      $task=$this->newtask;
      $user = $this->SearchAssagnee($task);
      \Mail::to($user)->send(new AssignmentMail($user, $task));

    }

    private function SearchAssagnee ($newtask) {
      $assagnee_name = $newtask->assignee;
      $use = User::where('name', $assagnee_name)->get();
      return   $use;
    }
}

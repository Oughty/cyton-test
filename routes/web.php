<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::get('/task/{sheet}/create', 'TaskController@create');
Route::post('/task/{sheet}/store', 'TaskController@store');
Route::get('/task/show/{task}', 'TaskController@show');
Route::any('/task/trash', 'TaskController@trash');
Route::any('/task/delete', 'TaskController@destroy');

/**
 * Department
 */
Route::get('/department/{department}/{priority}/', 'DepartmentController@show')->name('tasklist');
Route::get('/department/create/', 'DepartmentController@create');
Route::post('/department/store/', 'DepartmentController@store');

Auth::routes();
//Route::get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
Route::post('login', 'Auth\LoginController@login')->name('auth.login');
Route::post('logout', 'Auth\LoginController@logout')->name('auth.logout');

Route::get('/', 'HomeController@index')->name('home');
//
Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');

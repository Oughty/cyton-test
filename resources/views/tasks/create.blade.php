@extends('layouts.laratask')
<!-- Content Header (Page header) -->
@section('content')
<section class="content-header">
    <h1>
        {{$title}}
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="{{action('TaskController@store', ['department' => $department])}}">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Task Title</label>
                    <input type="text" name="title" class="form-control" placeholder="Enter ...">
                </div>

                <div class="form-group">
                    <label>Content</label>
                    <textarea class="form-control" name="content" rows="3" placeholder="Enter ..."></textarea>
                </div>

                <div class="form-group col-md-2">
                    <label>Task Due Date:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="dueDate" class="form-control datepicker" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                    </div>
                </div>

                <div class="form-group col-md-3">
                  <label> Assignee:</label>
                    <select  name="assignee" class="form-control">
                      <option value="">--Select Assignee----</option>
                      @foreach($assignees as $assignee)
                      <option value="{{$assignee->name}}">{{$assignee->name}}</option>
                     @endforeach
                    </select>
                  </div>

                <div class="form-group col-md-3">
                   <label> Followers:</label>
                     <select  name="" class="form-control">
                       <option value="">--Select Followers----</option>
                     </select>
                   </div>
                <div class="form-group col-md-4">
                    <label>Priority</label> <br>
                    @foreach($priorityOptions as $priorityOption)
                    <div class="radio inline">
                        <label>
                            <input type="radio" name="priority" id="optionsRadios1" value="{{$priorityOption}}" checked="">
                            {{ucwords($priorityOption)}}
                        </label>
                    </div>
                    @endforeach
                </div>
                <div class="clearfix"></div>
                <div class=" ">
                    <button type="submit" name="doAddTask" value="addTask" class="btn btn-primary">Add Task</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection

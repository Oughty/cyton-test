@extends('layouts.laratask')
<!-- Content Header (Page header) -->
@section('content')
<section class="content-header">
    <h1>

    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">

                <div class="form-group">
                    <label>Task Title</label>
                    <input type="text" name="title" class="form-control" value="{{$tasks->title}}">
                </div>

                <div class="form-group">
                    <label>Content</label>
                    <textarea class="form-control" name="content" rows="3">{{$tasks->content}}
                    </textarea>
                </div>

                <div class="form-group col-md-2">
                    <label>Task Due Date:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" value="{{$tasks->dueDate}}" name="dueDate" class="form-control datepicker" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                    </div>
                </div>

             <div class="form-group col-md-3">
                <label> Assignee:</label>
                  <select  name="assignee" class="form-control">
                    <option>{{$tasks->assignee}}</option>
                  </select>
                </div>

                <div class="form-group col-md-3">
                   <label> Followers:</label>
                     <select  name="assignee" class="form-control">
                       <option value="">--Select Followers----</option>
                     </select>
                   </div>
                <div class="form-group col-md-4">
                    <label>Priority</label> <br>
                    <div class="radio inline">
                        <label>
                            <input type="radio" name="priority" id="optionsRadios1" checked="">
                            {{ucwords($tasks->priority)}}
                        </label>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class=" ">
                    <button name="doAddTask" value="addTask" class="btn btn-primary">Edit Task</button>
                </div>
        </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection

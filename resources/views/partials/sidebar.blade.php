<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <!-- <img src="https://www.gravatar.com/avatar/{{md5( strtolower( trim( Auth::user()->email ) ) )}}" class="img-circle" alt="User Image"> -->
                <img src="/img/eric.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->email}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{{route('dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>

            </li>

            <li class="header">Departments</li>
            @can('Add Department')
            <li>
                <a href="{{action('DepartmentController@create')}}">
                    <i class="fa fa-plus"></i> <span>Add Department</span>
                </a>
            </li>
            @endcan
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>All Departments </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>

                </a>
                <ul class="treeview-menu">
                    <!-- Sheet menu list -->
                    @each('part.department-menu-item', $departments, 'department', 'part.task-empty')
                    <!-- /Sheet menu list -->
                </ul>
      @can('Manage Users')
                <li class="treeview">
    <a href="#">
        <i class="fa fa-users"></i>
        <span class="title">User Mngmt</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">

        <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
            <a href="{{ route('permissions.index') }}">
                <i class="fa fa-briefcase"></i>
                <span class="title">
                    Permissions
                </span>
            </a>
        </li>
        <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
            <a href="{{ route('roles.index') }}">
                <i class="fa fa-briefcase"></i>
                <span class="title">
                    Roles
                </span>
            </a>
        </li>
        <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
            <a href="{{ route('users.index') }}">
                <i class="fa fa-user"></i>
                <span class="title">
                    Users
                </span>
            </a>
        </li>
    </ul>
</li>
@endcan
        <li class="header">LABELS</li>
            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-sign-out"></i> <span>Logout</span>
                </a>

            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 0..4.0
    </div>
    <strong>Copyright &copy; 2994-3018 <a href="#">Cytonn Investments</a>.</strong> All rights
    reserved.
</footer>

@component('mail::message')
# Task Assignment

@component('mail::panel')
You are receiving this email because you have been assigned a task to work on,
{{$task}}.
Thanks,<br>{{ config('app.name') }}
@endcomponent

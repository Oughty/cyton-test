@extends('layouts.laratask')

@section('content')
    <h3 class="page-title">Roles</h3>

    {!! Form::model($role, ['method' => 'PUT', 'route' => ['roles.update', $role->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
           Edit
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
              @foreach ($permissions as $permission)
                 {{Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
                 {{Form::label($permission->name, ucfirst($permission->name)) }}<br>
              @endforeach
                    <p class="help-block"></p>
                    @if($errors->has('permission'))
                        <p class="help-block">
                            {{ $errors->first('permission') }}
                        </p>
                    @endif
                </div>
            </div>

        </div>
    </div>

    {!! Form::submit(trans('Update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

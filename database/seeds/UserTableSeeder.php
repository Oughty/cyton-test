<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Traits\HasPermissions;
use App\Permission;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     * @todo Should use Separate Eloquent Model Factory
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create();
        DB::table('users')->insert([
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime,
                'name' => 'Eric Otieno',
                'email' => 'eric@oti.com',
                'remember_token' => null,
                'password' => Hash::make('otieno')
            ]);

        for ($i = 1; $i <= 2; $i++) {
            $email=$faker->email;
            DB::table('users')->insert([
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime,
                'name' => $faker->name,
                'email' => $faker->email,
                'remember_token' => null,
                'password' => Hash::make(explode('@', $email)[0])
            ]);
        }

        // Seed the default permissions
        $permissions = Permission::defaultPermissions();

        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => $perms]);
        }

        $this->command->info('Default Permissions added.');


        $roles = Role::defaultRoles();

        foreach ($roles as $role) {
            $role = Role::firstOrCreate(['name' =>$role]);

            if ($role->name == 'admin') {
                $role->syncPermissions(Permission::all());
                $this->command->info('Admin granted all the permissions');
            } else {
                $role->syncPermissions(Permission::where('name', 'LIKE', 'View Tasks')->get());
            }
        }

        $this->command->info('Roles added successfully');


        $users=User::all();

        foreach ($users as $user) {
            $user->assignRole('admin');
        }
    }

  }

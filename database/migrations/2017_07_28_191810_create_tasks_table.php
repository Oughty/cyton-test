<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('dueDate');
            $table->dateTime('completetionDate')->nullable();
            $table->integer('department_id');
            $table->string('assignee');
            $table->enum('priority', ['urgent','high', 'medium', 'low']);

            $table->index('department_id');
            $table->index('priority');
            $table->index('dueDate');
            $table->index('completetionDate');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
